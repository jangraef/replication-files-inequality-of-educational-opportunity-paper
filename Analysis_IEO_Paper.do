/*==============================================================================
File name:    Analysis_IEO_Paper
Class:        Social and Gender Inequalities in Education
Author:		  Jan Graef 
Last update:  08.03.2022
==============================================================================*/

/*------------------------------------------------------------------------------
                     #1. Configuring dofile
------------------------------------------------------------------------------*/

* Setup
version 16.1            // Stata version control (put your own version)
clear all             // clear working memory
macro drop _all       // clear macros

* Working directory
global wdir  "C:\Users\graefjn\Desktop\Paper edu"


* Define paths to subdirectories (don't change anything here)
global data 		"$wdir/0_data"   		// folder for original data
global code 		"$wdir/1_dofiles"   	// folder for do-files
global posted 		"$wdir/2_posted"    	// data ready for analysis
global temp 		"$wdir/3_temp"   		// folder for temporary files
global table		"$wdir/4_tables" 		// folder for table output 
global graph		"$wdir/5_graphs" 		// folder for graph output 

* Install packages & ados 
ssc install fre
ssc install distinct
ssc install _gwtmean
ssc install sepscatter 
ssc install xtmrho
ssc install mdesc
ssc install estout

/*------------------------------------------------------------------------------
I PREPARATIONS
------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
                    1: Choosing Country sample 
------------------------------------------------------------------------------*/

/* Data: Download PISA 2018 data in Stata-format from the Evidence Institute: 
https://www.evidin.pl/en/data/ */

use "$data/PISA2018_students", clear

* Germany only 
keep if cntryid ==  276 // 606,553 observations deleted

/*------------------------------------------------------------------------------
                   2: Variable selection & preparation 
------------------------------------------------------------------------------*/

* I Student-level variables

* (i) School id variable: 
sort cntschid cntstuid
egen pickone = tag(cntschid)
gen sid = sum(pickone)
label variable sid "School ID"

* (ii) Student id variable 
rename cntstuid id 
label variable id "Student ID"

* (iii) Gender variable
fre st004d01t
recode st004d01t (1=1 "Female") (2=0 "Male"), gen(female)
label variable female "Female" 

* (iv) Reading score
rename pv1read rscore

* (v) Math score
rename pv1math mscore


* II School-level variables

* (i) Gender gap reading score

* a) Average boys score: 
bysort sid: egen aux_br = mean(rscore) if female == 0
bysort sid: egen rscore_b = min(aux_br)

* b) Average girls score: 
bysort sid: egen aux_gr = mean(rscore) if female == 1
bysort sid: egen rscore_g = min(aux_gr)

* c) Gender gap 
bysort sid: gen gap_rscore = rscore_b - rscore_g
label var gap_rscore "Gender gap: reading"


* (ii) Gender gap math score

* a) Average boys score: 
bysort sid: egen aux_bm = mean(mscore) if female == 0
bysort sid: egen mscore_b = min(aux_bm)

* b) Average girls score: 
bysort sid: egen aux_gm = mean(mscore) if female == 1
bysort sid: egen mscore_g = min(aux_gm)

* c) Gender gap 
bysort sid: gen gap_mscore = mscore_b - mscore_g
label var gap_mscore "Gender gap: math"

* (iii) Share of female students
bysort sid: egen s_female = mean(female)
label var s_female "Share female students"

* (iv) School type (recoded according to Mang et al. (2021: 16-18))

label define type 	1 "Hauptschule" 								///
					2 "School with multiple tracks"					///
					3 "Comprehensive school"						///
					4 "Realschule"									///
					5 "Gymnasium"									///
					6 "Vocational school"							///
					7 "Special-needs school"
					
gen type = .
replace type = 1 if inlist(progn,"02760002")
replace type = 2 if inlist(progn,"02760008","02760009","02760010","02760011" ///
								,"02760012","02760013","02760014","02760015")
replace type = 3 if inlist(progn,"02760006","02760007","02760016")
replace type = 4 if inlist(progn,"02760003")
replace type = 5 if inlist(progn,"02760004","02760005","02760021")
replace type = 6 if inlist(progn,"02760018","02760019","02760020")
replace type = 7 if inlist(progn,"02760001")

label values type type
label var type "School type"

/*------------------------------------------------------------------------------
                   3: Sample selection
------------------------------------------------------------------------------*/

* (i) Missing rates
*	a) School level variables 
		mdesc s_female type if pickone == 1
		* No missing values 
*	b) Individual level variables: 
		mdesc female rscore mscore 
		* No missing values

* (ii) Sample selection: 

* 	Initial sample size (Germany only, n=students, m=schools): 	n = 5451
		distinct id sid //										m =  223

* Drop if strict single-sex schools
	drop if s_female == 0 | s_female == 1 // 49 obs. deleted 	n = 5402
		distinct id sid // 5 schools deleted					m =  218
		
*	Analytic sample size										n = 5402
		distinct id sid // 										m =  218
		

/*------------------------------------------------------------------------------
                   4: Univariate & bivariate distributions 
------------------------------------------------------------------------------*/

* (i) Reading scores across schools
by sid, sort: egen rscore_smean= mean(rscore)
label variable rscore_smean "Unweighted school mean reading score"
hist rscore_smean if pickone == 1, percent normal
sum rscore_smean if pickone == 1, d
/* Across the schools in the sample, the distribution of mean reading scores is 
left-skewed. */

* (ii) Math scores across schools
by sid, sort: egen mscore_smean= mean(mscore)
label variable mscore_smean "Unweighted school mean math score"
hist mscore_smean if pickone == 1, percent normal
sum mscore_smean if pickone == 1, d
/* Across the schools in the sample, the distribution of mean math scores is 
left-skewed. */

* (iii) Reading scores by gender: 
bysort female: sum rscore
twoway 	(histogram rscore if female == 0, ///
		percent color(green%30)) ///
		(histogram rscore if female == 1, ///
		percent color(red%30)), ///
		legend(order(1 "Boys" 2 "Girls"))
graph export "$graph/g1.pdf", replace

* (iv) Reading scores by gender: 
bysort female: sum mscore
twoway 	(histogram mscore if female == 0, ///
		percent color(green%30)) ///
		(histogram mscore if female == 1, ///
		percent color(red%30)), ///
		legend(order(1 "Boys" 2 "Girls"))
graph export "$graph/g2.pdf", replace

* (v) Gender differences in mean reading scores across schools
hist gap_rscore if pickone == 1, percent
sum gap_rscore if pickone == 1, d

* (vi) Gender differences in mean math scores across schools
hist gap_mscore if pickone == 1, percent
sum gap_mscore if pickone == 1, d

/* (vii) Differences in reading scores by share of females in school */
twoway 	(histogram rscore_smean ///
		if s_female < 0.5 & pickone == 1, ///
		freq color(green%30)) ///
		(histogram rscore_smean ///
		if s_female >= 0.5 & pickone == 1, ///
		freq color(red%30)), ///
		legend(order(1 "More boys" 2 "More girls"))
graph export "$graph/g3.pdf", replace

/* (viii) Differences in math scores by share of females in school */
twoway 	(histogram mscore_smean ///
		if s_female < 0.5 & pickone == 1, ///
		freq color(green%30)) ///
		(histogram mscore_smean ///
		if s_female >= 0.5 & pickone == 1, ///
		freq color(red%30)), ///
		legend(order(1 "More boys" 2 "More girls"))
graph export "$graph/g4.pdf", replace

/* (ix) Differences in reading gender gap by share of females in school */
twoway 	(histogram gap_rscore ///
		if s_female < 0.5 & pickone == 1, ///
		freq color(green%30)) ///
		(histogram gap_rscore ///
		if s_female >= 0.5 & pickone == 1, ///
		freq color(red%30)), ///
		legend(order(1 "More boys" 2 "More girls"))
graph export "$graph/g5.pdf", replace

/* (x) Differences in math gender gap by share of females in school */
twoway 	(histogram gap_mscore ///
		if s_female < 0.5 & pickone == 1, ///
		freq color(green%30)) ///
		(histogram gap_mscore ///
		if s_female >= 0.5 & pickone == 1, ///
		freq color(red%30)), ///
		legend(order(1 "More boys" 2 "More girls"))
graph export "$graph/g6.pdf", replace

* (xi) Gender composition by school type
table type if pickone == 1, c(mean s_female) // important confounder!

* Save prepared dataset

save "$posted/Analytic", replace

/*-----------------------------------------------------------------------------
II SUMMARY STATISTICS
-----------------------------------------------------------------------------*/

clear all             			
	use "$posted/Analytic", clear

* (i) Generate dummy variables and relabel

label var rscore "Reading Score"
label var mscore "Math Score"

age

* School type
tab type, gen(t)
label var t1 "\hspace{0.25cm} Hauptschule"
label var t2 "\hspace{0.25cm} Multiple track"
label var t3 "\hspace{0.25cm} Comprehensive"
label var t4 "\hspace{0.25cm} Realschule"
label var t5 "\hspace{0.25cm} Gymnasium"
label var t6 "\hspace{0.25cm} Vocational"
label var t7 "\hspace{0.25cm} Special-needs"

* (ii) Generate table

* Table 1: Descriptives (level 1 variables)
eststo table1: estpost tabstat 											///
	rscore mscore age t1 - t7, by(female)								///
	statistics(mean sd min max) nototal columns(statistics)

esttab table1 using "$table/table1.tex", compress label replace			///
 refcat(	t1 "\emph{School type}"										///
			, nolabel) 													///
 cells("mean(fmt(2)) sd(fmt(2)) min(fmt(2)) max(fmt(2))") 				///
 collabels("M" "SD" "Min" "Max")										///
 mtitle ("Boys" "Girls") 												///
 wide not nostar unstack nonumber noobs nodepvars						///
 note()  

* Table 2: Descriptives (level 2 variables)
eststo table2: estpost tabstat 											///
	s_female t1 - t7 if pickone == 1,									///
	statistics(mean sd min max) columns(statistics)

esttab table2 using "$table/table2.tex", compress label replace			///
 refcat(	t1 "\emph{School type}"										///
			, nolabel) 													///
 cells("mean(fmt(2)) sd(fmt(2)) min(fmt(2)) max(fmt(2))") 				///
 collabels("M" "SD" "Min" "Max")										///
 mtitle ("Schools") 													///
 wide not nostar unstack nonumber noobs nodepvars						///
 note()  


/*-----------------------------------------------------------------------------
III ESTIMATING MULTILEVEL MODELS 
-----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                   1. Reading scores
-----------------------------------------------------------------------------*/

* Model M0: Empty model for reading scores
xtmixed rscore || sid:, mle var
xtmrho
scalar l2varm0r = e(var_u1)
scalar l1varm0r = e(var_e)
display l2varm0r / (l2varm0r + l1varm0r)
display l1varm0r / (l2varm0r + l1varm0r)
est store M0r
* Calculate variance components and add them to stored estimates
ereturn list
mat list e(b)
estadd scalar l2_var_r = exp(2*[lns1_1_1]_b[_cons]), replace
estadd scalar l1_var_r = exp(2*[lnsig_e]_b[_cons]), replace


/* Model M1: Random intercept model with 
	-Level-1 binary indicator for girls */
xtmixed rscore i.female || sid:, mle var
xtmrho
scalar l2varm1r = e(var_u1)
scalar l1varm1r = e(var_e)
display (l2varm0r - l2varm1r)/l2varm0r
display (l1varm0r - l1varm1r)/l1varm0r
est store M1r
estadd scalar l2_var_r = exp(2*[lns1_1_1]_b[_cons]), replace
estadd scalar l1_var_r = exp(2*[lnsig_e]_b[_cons]), replace

/* Model M2: Random intercept model with 
	-Level-1 binary indicator for girls
	-Level-2 share of female students in school*/
xtmixed rscore i.female c.s_female || sid:, mle var
xtmrho
scalar l2varm2r = e(var_u1)
scalar l1varm2r = e(var_e)
display (l2varm1r - l2varm2r)/l2varm0r
display (l1varm1r - l1varm2r)/l1varm0r
est store M2r
estadd scalar l2_var_r = exp(2*[lns1_1_1]_b[_cons]), replace
estadd scalar l1_var_r = exp(2*[lnsig_e]_b[_cons]), replace

/* Model M3: Random intercept model with 
	-Level-1 binary indicator for girls
	-Level-2 share of female students in school
	-Control school type*/
xtmixed rscore i.female c.s_female i.type || sid:, mle var
xtmrho
scalar l2varm3r = e(var_u1)
scalar l1varm3r = e(var_e)
display (l2varm2r - l2varm3r)/l2varm0r
display (l1varm2r - l1varm3r)/l1varm0r
est store M3r
estadd scalar l2_var_r = exp(2*[lns1_1_1]_b[_cons]), replace
estadd scalar l1_var_r = exp(2*[lnsig_e]_b[_cons]), replace

/* Model M4: Random slope model with 
	-Level-1 binary indicator for girls
	-Level-2 share of female students in school
	-control variables (school type)
	-unstructured covariance	*/
xtmixed rscore i.female c.s_female i.type || sid:female, ///
	mle var cov(unstructured)
xtmrho
scalar l2varm4r = e(var_u1)
scalar l1varm4r = e(var_e)
display (l2varm3r - l2varm4r)/l2varm0r // approx. 0 
display (l1varm3r - l1varm4r)/l1varm0r // approx. 0 
est store M4r
estadd scalar l2_var_r = exp(2*[lns1_1_2]_b[_cons]), replace 
estadd scalar l1_var_r = exp(2*[lnsig_e]_b[_cons]), replace
estadd scalar slope_var_r = exp(2*[lns1_1_1]_b[_cons]), replace 
estadd scalar cov_unstruc_r = tanh([atr1_1_1_2]_b[_cons])* ///
	exp([lns1_1_1]_b[_cons])* exp([lns1_1_2]_b[_cons]), replace


* Test for model fit improvement:
lrtest M3r M4r

* Calculating 95% coverage interval for gender effect on reading scores: 
ereturn list
matrix list e(b)
scalar mfemale2 = _b[rscore:1.female]
matrix list e(V)
scalar vfemale2 = exp(_b[lns1_1_1:_cons])^2 
display vfemale2
display mfemale2 + 1.96* sqrt(vfemale2) // 27.56
display mfemale2 - 1.96* sqrt(vfemale2) // -6.55

*Graphical depiction of slopes across schools: 
capture drop yhat
predict yhat2, fitted
sort sid female
twoway line yhat2 female, 		///
	connect(ascending)		///
	lwidth(vvthin)			///
	ylab(100(100)800, angle(0))
	
/* Model M5: Random slope model with 
	-Level-1 binary indicator for girls
	-Level-2 share of female students in school
	-cross level interaction between female and share of female students
	-control variables (school type)
	-unstructured covariance 												*/
xtmixed rscore i.female##c.s_female i.type ///
	|| sid:female, mle var cov(unstructured)
xtmrho
scalar l2varm5r = e(var_u1)
scalar l1varm5r = e(var_e)
display (l2varm4r - l2varm5r)/l2varm0r // approx. 0 
display (l1varm4r - l1varm5r)/l1varm0r // approx. 0 
est store M5r
estadd scalar l2_var_r = exp(2*[lns1_1_2]_b[_cons]), replace 
estadd scalar l1_var_r = exp(2*[lnsig_e]_b[_cons]), replace
estadd scalar slope_var_r = exp(2*[lns1_1_1]_b[_cons]), replace 
estadd scalar cov_unstruc_r = tanh([atr1_1_1_2]_b[_cons])* ///
	exp([lns1_1_1]_b[_cons])* exp([lns1_1_2]_b[_cons]), replace

margins, at (female=(0 1) s_female=(0.05(0.4)0.85)) post noestimcheck

* Figure 1a): Marginsplot for reading
marginsplot, ///
	title ("{bf:a)} Reading", size(med) color(black))				///
	ytitle("Reading score", size(small)) 							///
	ylab (440 (20) 540, labsize(small) angle(0) 					///
		grid glcolor(gs15) gmin gmax) 								///
	xtitle("{bf:Gender}", size(small)) 								///
	xscale(titlegap(1) ) xlab (,labsize(small) labgap(1) angle(0)) 	///
	legend(order(1 "0.05" 2 "0.45" 3 "0.85")						///
	cols(1) title("Share female students", size(med) color(black))) ///
	graphregion(color(white) fcolor(white) icolor(white))     		///
	name(marginsa, replace) xsize(4) note("")

* Table 3: Model results for reading	
esttab M0r M1r M2r M3r M4r M5r using "$table/table3.tex", se label nobaselevels	/// 
	stats(l2_var_r l1_var_r slope_var_r cov_unstruc_r,					///
	fmt(%9.2f %9.2f %9.2f %9.2f) 							///
		labels("L2 Var: Schools" "L1 Var: Individual" 					///
		"Slope Variance" "Covariance(unstruc.)" )) 		///
	drop(lns1_1_1:_cons lnsig_e:_cons lns1_1_2:_cons atr1_1_1_2:_cons) 	///
	mtitles ("M0" "M1" "M2" "M3" "M4" "M5") nonumbers								///
	title("Reading") replace	
	

/*-----------------------------------------------------------------------------
                   1. Math scores
-----------------------------------------------------------------------------*/

* Model M0: Empty model for math scores
xtmixed mscore || sid:, mle var
xtmrho
scalar l2varm0m = e(var_u1)
scalar l1varm0m = e(var_e)
display l2varm0m / (l2varm0m + l1varm0m)
display l1varm0m / (l2varm0m + l1varm0m)
est store M0m
* Calculate variance components and add them to stored estimates
ereturn list
mat list e(b)
estadd scalar l2_var_m = exp(2*[lns1_1_1]_b[_cons]), replace
estadd scalar l1_var_m = exp(2*[lnsig_e]_b[_cons]), replace


/* Model M1: Random intercept model with 
	-Level-1 binary indicator for girls */
xtmixed mscore i.female || sid:, mle var
xtmrho
scalar l2varm1m = e(var_u1)
scalar l1varm1m = e(var_e)
display (l2varm0m - l2varm1m)/l2varm0m
display (l1varm0m - l1varm1m)/l1varm0m
est store M1m
estadd scalar l2_var_m = exp(2*[lns1_1_1]_b[_cons]), replace
estadd scalar l1_var_m = exp(2*[lnsig_e]_b[_cons]), replace

/* Model M2: Random intercept model with 
	-Level-1 binary indicator for girls
	-Level-2 share of female students in school*/
xtmixed mscore i.female c.s_female || sid:, mle var
xtmrho
scalar l2varm2m = e(var_u1)
scalar l1varm2m = e(var_e)
display (l2varm1m - l2varm2m)/l2varm0m
display (l1varm1m - l1varm2m)/l1varm0m
est store M2m
estadd scalar l2_var_m = exp(2*[lns1_1_1]_b[_cons]), replace
estadd scalar l1_var_m = exp(2*[lnsig_e]_b[_cons]), replace

/* Model M3: Random intercept model with 
	-Level-1 binary indicator for girls
	-Level-2 share of female students in school
	-Control school type*/
xtmixed mscore i.female c.s_female i.type || sid:, mle var
xtmrho
scalar l2varm3m = e(var_u1)
scalar l1varm3m = e(var_e)
display (l2varm2m - l2varm3m)/l2varm0m
display (l1varm2m - l1varm3m)/l1varm0m
est store M3m
estadd scalar l2_var_m = exp(2*[lns1_1_1]_b[_cons]), replace
estadd scalar l1_var_m = exp(2*[lnsig_e]_b[_cons]), replace

/* Model M4: Random slope model with 
	-Level-1 binary indicator for girls
	-Level-2 share of female students in school
	-control variables (school type)
	-unstructured covariance	*/
xtmixed mscore i.female c.s_female i.type || sid:female, ///
	mle var cov(unstructured)
xtmrho
scalar l2varm4m = e(var_u1)
scalar l1varm4m = e(var_e)
display (l2varm3m - l2varm4m)/l2varm0m // approx. 0 
display (l1varm3m - l1varm4m)/l1varm0m // approx. 0 
est store M4m
estadd scalar l2_var_m = exp(2*[lns1_1_2]_b[_cons]), replace 
estadd scalar l1_var_m = exp(2*[lnsig_e]_b[_cons]), replace
estadd scalar slope_var_m = exp(2*[lns1_1_1]_b[_cons]), replace 
estadd scalar cov_unstruc_m = tanh([atr1_1_1_2]_b[_cons])* ///
	exp([lns1_1_1]_b[_cons])* exp([lns1_1_2]_b[_cons]), replace


* Test for model fit improvement:
lrtest M3m M4m

* Calculating 95% coverage interval for gender effect on reading scores: 
ereturn list
matrix list e(b)
scalar mfemale2m = _b[mscore:1.female]
matrix list e(V)
scalar vfemale2m = exp(_b[lns1_1_1:_cons])^2 
display vfemale2m
display mfemale2m + 1.96* sqrt(vfemale2m) // -9.68
display mfemale2m - 1.96* sqrt(vfemale2m) // -31.57

*Graphical depiction of slopes across schools: 
capture drop yhat
predict yhat2, fitted
sort sid female
twoway line yhat2 female, 		///
	connect(ascending)		///
	lwidth(vvthin)			///
	ylab(100(100)800, angle(0))
	
/* Model M5: Random slope model with 
	-Level-1 binary indicator for girls
	-Level-2 share of female students in school
	-cross level interaction between female and share of female students
	-control variables (school type)
	-unstructured covariance 												*/
xtmixed mscore i.female##c.s_female i.type ///
	|| sid:female, mle var cov(unstructured)
xtmrho
scalar l2varm5m = e(var_u1)
scalar l1varm5m = e(var_e)
display (l2varm4m - l2varm5m)/l2varm0m // approx. 0 
display (l1varm4m - l1varm5m)/l1varm0m // approx. 0 
est store M5m
estadd scalar l2_var_m = exp(2*[lns1_1_2]_b[_cons]), replace 
estadd scalar l1_var_m = exp(2*[lnsig_e]_b[_cons]), replace
estadd scalar slope_var_m = exp(2*[lns1_1_1]_b[_cons]), replace 
estadd scalar cov_unstruc_m = tanh([atr1_1_1_2]_b[_cons])* ///
	exp([lns1_1_1]_b[_cons])* exp([lns1_1_2]_b[_cons]), replace

margins, at (female=(0 1) s_female=(0.05(0.4)0.85)) post noestimcheck

* Figure 1b): Marginspolot for math
marginsplot, ///
	title ("{bf:b)} Math", size(med) color(black))				///
	ytitle("Math score", size(small)) 							///
	ylab (440 (20) 540, labsize(small) angle(0) 					///
		grid glcolor(gs15) gmin gmax) 								///
	xtitle("{bf:Gender}", size(small)) 								///
	xscale(titlegap(1) ) xlab (,labsize(small) labgap(1) angle(0)) 	///
	legend(order(1 "0.05" 2 "0.45" 3 "0.85")						///
	cols(1) title("Share female students", size(med) color(black))) ///
	graphregion(color(white) fcolor(white) icolor(white))     		///
	name(marginsb, replace) xsize(4) note("")

* Figure 1: Combine panels	
 grc1leg marginsa marginsb, graphregion(color(white)) caption("") ///
 xsize(6) note("{it:Source}: PISA 2018, German sample.")
graph export "$graph/Figure1.png", replace
	
* Table 4: Model results for math
esttab M0m M1m M2m M3m M4m M5m using "$table/table4.tex", se label nobaselevels	/// 
	stats(l2_var_m l1_var_m slope_var_m cov_unstruc_m,					///
	fmt(%9.2f %9.2f %9.2f %9.2f) 							///
		labels("L2 Var: Schools" "L1 Var: Individual" 					///
		"Slope Variance" "Covariance(unstruc.)" )) 		///
	drop(lns1_1_1:_cons lnsig_e:_cons lns1_1_2:_cons atr1_1_1_2:_cons) 	///
	mtitles ("M0" "M1" "M2" "M3" "M4" "M5") nonumbers								///
	title("Math") replace

	
***********	
exit